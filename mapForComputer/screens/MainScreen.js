import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import React, { useState, useEffect } from 'react';
import { View, TextInput, TouchableOpacity, Text, StyleSheet, Button,ScrollView,SafeAreaView} from 'react-native';
import { SelectList } from 'react-native-dropdown-select-list';
import DateTimePickerModal from "react-native-modal-datetime-picker";
// import MapView, { Marker } from 'react-native-maps';
import Modal from "react-native-modal";
 import { Camera } from 'expo-camera';
 import dayjs from "dayjs";
 import * as MailComposer from 'expo-mail-composer';
 import * as Location from 'expo-location';

export default function MainScreen() {



  
  const [mapRegion, setMapRegion] = useState({
    latitude: 46.022464,
    longitude: 2.000049,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const userLocation = async () => {
    let {status} = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
      setErrorMsg('Permission to access location was denied');
    }
    let location = await Location.getCurrentPositionAsync({enableHighAccuracy: true});
    setMapRegion({
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    });
    setLatitude(location.coords.latitude)
    setLongitude(location.coords.longitude)
    console.log(location.coords.latitude, location.coords.longitude);
  }

  

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [message, setMessage] = useState('');
  const [adress, setAdress] = useState('');
  const [cp, setCP] = useState('');
  const [city, setCity] = useState('');
  const [mail, setMail] = useState('');
  const [phone, setPhone] = useState('');
  const [alert, setAlert] = useState('');
  const [date, setDate] = useState('');
  const [recipient, setRecipient] = useState('');
  const [longitude, setLongitude] = useState('');
    const [latitude, setLatitude] = useState('');
  const dateString = dayjs(date).format("MM/DD/YYYY HH:mm:ss A");
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [camera, setCamera] = useState(null);
  const [image, setImage] = useState(null);
  const [isAvailable, setIsAvailable] = useState(false);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const subject = alert;

  const body ="Nom: "+ lastName +"/ Prénom: "+ firstName + "/ Adresse "+adress+"/ Code postale "+cp+"/ Ville: "+city+ "/ Email: "+mail+"/ Numéro "+phone+"/Date: "+date+"/ Description: "+message+"/ latitude: "+latitude+ "/ longitude: "+longitude; 
  const knowMail = async () => {
        if(selected == "Voirie"){
          setRecipient("voirie@simplon.com")
        }
        if(selected == "Stationnement"){
          setRecipient("stationnement@simplon.com")
        }
        if(selected == "Travaux"){
          setRecipient("Travaux@simplon.com")
        }
  }
useEffect(() => {
    (async () => {
      const cameraStatus = await Camera.requestCameraPermissionsAsync();
      setHasCameraPermission(cameraStatus.status === 'granted');
})();

async function checkAvailability() {
  const isMailAvailable = await MailComposer.isAvailableAsync();
  setIsAvailable(isMailAvailable);
}

checkAvailability();
userLocation();
  }, []);
const takePicture = async () => {
    if(camera){
        const data = await camera.takePictureAsync(null)
        setImage(data.uri);
    }
  }

  if (hasCameraPermission === false) {
    return <Text>Pas d'accès à la caméra</Text>;
  }

  const sendMail = async () => {


    MailComposer.composeAsync({
      subject: selected,
      body: body,
      recipients: recipient,
      attachments: [image]
    });
  };

const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

const showDatePicker = () => {
  setDatePickerVisibility(true);
};

const hideDatePicker = () => {
  setDatePickerVisibility(false);
};

const handleConfirm = (date) => {
  setDate(date);
  console.warn("Date choisi: ", date);
  hideDatePicker();
};

const [isModalVisible, setModalVisible] = useState(false);

const toggleModal = () => {
  setModalVisible(!isModalVisible);
};

  const [selected, setSelected] = React.useState("");
  const [key, setKey] = React.useState("");
  
  const data = [
      {key:'1', value:'Voirie'},
      {key:'2', value:'Stationnement'},
      {key:'3', value:'Travaux'},
  ]
  return (

    <View  style={styles.container}>
      
      {/* <ScrollView> */}

      <Text>Simplonville</Text>
            <SelectList 
              placeholder="Type d'alerte"
              setSelected={(val) => setSelected(val)} 
              onSelect={knowMail}
              data={data} 
              save="value"
                  /> 
            <TextInput
              placeholder="Description"
              textAlignVertical="top"
              multiline={true}
              onChangeText={(value) => setMessage(value)}
              style={{
                padding: 15,
                height: 200,
                borderColor: 'black',
                borderWidth: 1,
              }}
            />

                  <Button title="Choisir la date" onPress={showDatePicker} />
                  <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                  />
              {/* <Button title="Choisir le lieu" onPress={toggleModal} />
             <Modal isVisible={isModalVisible}>

             <MapView style={styles.map}>
             <Marker coordinate={mapRegion} title="marker" />
             </MapView>
             <TextInput
                style={styles.input}
                keyboardType='numeric'
                placeholder="Latitude"
                onChangeText={(value) => setLatitude(value)}
            />
            <TextInput
                style={styles.input}
                keyboardType='numeric'
                placeholder="Longitude"
                onChangeText={(value) => setLongitude(value)}
                
            />
            <Button 
              title='georeverse'
              onPress={() =>setMapRegion({
                latitude: latitude,
                longitude: longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              })  } /> 
  

              <Button 
              title='Annuler'
              onPress={() => setModalVisible(false)} />
              </Modal>   */}
            
              <Camera   ref={ref => setCamera(ref)} style={styles.camera} type={type} ratio={'5:5'} >
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              setType(
                type === Camera.Constants.Type.back
                  ? Camera.Constants.Type.front
                  : Camera.Constants.Type.back
              );
            }}>
            <Text style={styles.text}> Inverser l'écran </Text>
          </TouchableOpacity>
        </View>
      </Camera>
      <Text>{image}</Text>
      <Button title="Prendre une photo" onPress={() => takePicture()} />
            <TextInput
                style={styles.input}
                placeholder="Nom"
                onChangeText={(value) => setLastName(value)}
            />
            <TextInput
                style={styles.input}
                placeholder="Prénom"
                onChangeText={(value) => setFirstName(value)}
            />
            <TextInput
                style={styles.input}
                placeholder="Adresse"
                onChangeText={(value) => setAdress(value)}
            />
            <TextInput
                style={styles.input}
                placeholder="Code Postale"
                onChangeText={(value) => setCP(value)}
            />
            <TextInput
                style={styles.input}
                placeholder="Ville"
                onChangeText={(value) => setCity(value)}
            />
            <TextInput
                style={styles.input}
                placeholder="Email"
                onChangeText={(value) => setMail(value)}
            />
            <TextInput
                style={styles.input}
                placeholder="Téléphone"
                onChangeText={(value) => setPhone(value)}
            />

            <Button 
            title='Submit'
            onPress={sendMail} 
            />
 {isAvailable ? <Button title='Send Mail' onPress={sendMail} /> : <Text>Email not available</Text>}
      <StatusBar style="auto" />
      {/* </ScrollView> */}

    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '60'
  },
  camera: {
    width: '50%',
    height: '50%',
  },
    map: {
    width: '100%',
    height: '50%',
  },
});
